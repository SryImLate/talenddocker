FROM java:8-jre

ARG talend_job=testjob
ARG talend_version=0.1

LABEL maintainer="antoineperault@gmail.com" \
    talend.job=${talend_job} \
    talend.version=${talend_version}

ENV TALEND_JOB ${talend_job}
ENV TALEND_VERSION ${talend_version}
ENV ARGS ""

WORKDIR /opt/app

COPY ${TALEND_JOB}_${talend_version}.zip .

### Install Talend Job
RUN apt-get install unzip && \
    unzip ${TALEND_JOB}_${TALEND_VERSION}.zip && \
    rm -rf ${TALEND_JOB}_${TALEND_VERSION}.zip && \
    chmod +x ${TALEND_JOB}/${TALEND_JOB}_run.sh

VOLUME /data

CMD ["/bin/sh","-c","${TALEND_JOB}/${TALEND_JOB}_run.sh ${ARGS}"]